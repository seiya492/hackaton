import { LitElement, html } from 'lit-element';
import '../atomic-login/atomic-login.js'
import '../atomic-banner/atomic-banner.js'
import sandbox from '../sandbox/sandbox';
import ejecuteService from '../ajax/ajax';

class AtomicApp extends LitElement {

    static get properties() {
        return {
            users: { type: Array },
            url: { type: String }
        };
    }

    constructor() {
        super();
        this.users = [];
        this.url = 'http://localhost:3000/Usuarios';

        this.getUsers();
        sandbox.on('validate-login',this.validateLogin.bind(this));
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <atomic-login></atomic-login>
            <atomic-banner></atomic-banner>
        `;
    }

    getUsers(){
        ejecuteService.ajax('GET', this.url, function(response){
			this.users=response;
		}.bind(this));
    }

    validateLogin(e){
        let resultado = this.users.find( user => user.nick === e.detail.user.nick &&  user.password === e.detail.user.passwd);
        
        if(resultado === undefined){
            sandbox.dispatch('show-login',{'user':this.user},this);
        }
        else{
            sandbox.dispatch('data-user',{'user': resultado},this);
            console.log("banner");
            sandbox.dispatch('show-banner', {},this);
            console.log(resultado);
        }
    }

}

customElements.define('atomic-app', AtomicApp)